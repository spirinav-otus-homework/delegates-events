﻿using System;
using System.Collections.Generic;

namespace DelegatesAndEvents
{
	public static class EnumerableExtensions
	{
		public static T GetMax<T>(this IEnumerable<T> e, Func<T, float> getParameter)
		{
			float? maxValue = null;
			T maxElement = default;

			foreach (var currElement in e)
			{
				var value = getParameter(currElement);

				if (maxValue >= value)
				{
					continue;
				}

				maxValue = value;
				maxElement = currElement;
			}

			return maxElement;
		}
	}
}