﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;

namespace DelegatesAndEvents
{
    public class FileTraverser
    {
        private readonly string _directoryPath;
        private readonly CancellationToken _cancellationToken;

        public Dictionary<DirectoryInfo, List<FileInfo>> FoundFiles = new();

        public event EventHandler<FileArgs> FileFound;

        public FileTraverser(string directoryPath, CancellationToken cancellationToken = default)
        {
            _directoryPath = string.IsNullOrWhiteSpace(directoryPath)
                ? Environment.CurrentDirectory
                : directoryPath;
            _cancellationToken = cancellationToken;
        }

        public FileTraverser() : this(default, default) { }

        public FileTraverser(CancellationToken cancellationToken) : this(default, cancellationToken) { }

        public void Traversal(string directoryPath = default)
        {
            var dirInfo = new DirectoryInfo(directoryPath ?? _directoryPath);
            if (!dirInfo.Exists)
            {
                return;
            }

            var subDirInfo = dirInfo.GetDirectories().OrderBy(x => x.Name);
            foreach (var sdi in subDirInfo)
            {
                if (_cancellationToken.IsCancellationRequested)
                {
                    return;
                }

                Traversal(sdi.FullName);
            }

            var filesInfo = dirInfo.GetFiles().OrderBy(x => x.Name);
            foreach (var fi in filesInfo)
            {
                if (_cancellationToken.IsCancellationRequested)
                {
                    return;
                }

                if (!FoundFiles.ContainsKey(dirInfo))
                {
                    FoundFiles.Add(dirInfo, new List<FileInfo>());
                }

                FoundFiles[dirInfo].Add(fi);
                FileFound?.Invoke(this, new FileArgs(fi.FullName));
            }
        }
    }
}