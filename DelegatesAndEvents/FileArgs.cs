﻿using System;

namespace DelegatesAndEvents
{
    public class FileArgs : EventArgs
    {
        public FileArgs(string fileName)
        {
            FileName = fileName;
        }

        public string FileName { get; }
    }
}