﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace DelegatesAndEvents
{
    class Program
    {
        #region MessageTexts
        
        private const string InstructionText = "FileTraverser" +
                                               "\n* Set directory and press Enter to files traversal in specified directory." +
                                               "\n* Just press Enter to files traversal in default directory." +
                                               "\n* Press Esc to cancel execution.";
        private const string TraversalStartText = "\nFiles traversal in specified directory started.\n";
        private const string TraversalCompletedText = "\nFiles traversal has been successfully completed.";
        private const string TraversalCancelText = "\nFiles traversal has been canceled.";
        private const string TraversalResultSummaryText = "\nFiles traversal summary:\n* Found {0} files in {1} directories.";
        private const string TraversalResultMaxDirText = "* Directory {0} includes the largest number of files: {1}.";
        private const string TraversalResultMaxFileText = "* File {0} has the largest size: {1} bytes.";
        
        #endregion

        // ReSharper disable once UnusedParameter.Local
        static void Main(string[] args)
        {
            var cancelTokenSource = new CancellationTokenSource();
            var cancelToken = cancelTokenSource.Token;

            OutputMessage(InstructionText);

            var filesTraversalTask = Task.Run(() =>
            {
                var fileTraverser = new FileTraverser(Console.ReadLine(), cancelToken);

                OutputMessage(TraversalStartText);
                
                fileTraverser.FileFound += OnFileFound;
                fileTraverser.Traversal();
                fileTraverser.FileFound -= OnFileFound;

                if (cancelToken.IsCancellationRequested)
                {
                    return fileTraverser.FoundFiles;
                }

                OutputMessage(TraversalCompletedText);

                return fileTraverser.FoundFiles;
            }, cancelToken);

            Task.Run(() =>
            {
                while (true)
                {
                    if (Console.ReadKey(true).Key != ConsoleKey.Escape) continue;

                    cancelTokenSource.Cancel();
                    OutputMessage(TraversalCancelText);
                    break;
                }
            }, cancelToken);

            var dirCount = filesTraversalTask.Result.Count;
            var fileCount = filesTraversalTask.Result.SelectMany(x => x.Value).Count();
            OutputResult(string.Format(TraversalResultSummaryText, fileCount, dirCount));

            var largestDir = filesTraversalTask.Result.GetMax(x => x.Value.Count);
            if (largestDir.Key != null)
            {
                OutputResult(string.Format(TraversalResultMaxDirText, largestDir.Key.FullName, largestDir.Value.Count));
            }

            var largestFile = filesTraversalTask.Result.SelectMany(x => x.Value).GetMax(x => x.Length);
            if (largestFile != null)
            {
                OutputResult(string.Format(TraversalResultMaxFileText, largestFile.FullName, largestFile.Length));
            }

            Console.ReadLine();
        }

        private static void OutputMessage(string message)
        {
            Console.ForegroundColor = ConsoleColor.DarkYellow;
            Console.WriteLine(message);
            Console.ResetColor();
        }

        private static void OutputResult(string result)
        {
            Console.ForegroundColor = ConsoleColor.DarkGreen;
            Console.WriteLine(result);
            Console.ResetColor();
        }

        private static void OnFileFound(object sender, FileArgs e)
        {
            OutputResult(e.FileName);
        }
    }
}